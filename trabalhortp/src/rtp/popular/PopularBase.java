package rtp.popular;

import rtp.exceptions.ValidacaoException;
import rtp.model.IManager;
import rtp.model.TarefaManager;
import rtp.model.entity.Tarefa;
import rtp.utils.Validacoes;

public class PopularBase {

	private GerarResponsavel gerarResponsavel;
	private GerarTarefa gerarTarefa;
	
	public PopularBase() {
		// TODO Auto-generated constructor stub
		gerarResponsavel = new GerarResponsavel();
		gerarTarefa = new GerarTarefa();
	}
	
	public GerarResponsavel getGerarResponsavel() {
		return gerarResponsavel;
	}
	
	public GerarTarefa getGerarTarefa() {
		return gerarTarefa;
	}
	
	public final void execute () {
		
		try {
			
			if (!Validacoes.isEmpty(gerarTarefa.getTarefaManager().getAll())) {
				System.out.println("Base contem objectos. return.");
				return;
			}
			
			gerarResponsavel.execute();
			gerarTarefa.execute();
			
		} catch (ValidacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {

		PopularBase popularBase = new PopularBase();
		popularBase.execute();

		IManager<Tarefa> tarefaManager = new TarefaManager();

		for (Tarefa r : tarefaManager.getAll()) {

			System.out.println(r);

		}		

	}
	
}

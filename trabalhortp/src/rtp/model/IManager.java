package rtp.model;

import java.util.LinkedHashSet;

import rtp.exceptions.ValidacaoException;


public interface IManager<T> {

	public T cadastrar (T t) throws ValidacaoException;
	
	public T getByCodigo (long codigo);
	
	public LinkedHashSet<T> getAll ();
	
}

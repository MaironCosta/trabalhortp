package rtp.model.teste;

import org.junit.Assert;
import org.junit.Test;

import rtp.exceptions.ValidacaoException;
import rtp.model.ResponsavelManager;
import rtp.model.entity.Responsavel;

public class TesteResponsavelManager {

	private ResponsavelManager responsavelManager;
	
	public TesteResponsavelManager() {
		// TODO Auto-generated constructor stub
		
		responsavelManager = new ResponsavelManager();
		
	}
	
	public Responsavel getMock () {

		Responsavel responsavel = new Responsavel();
		responsavel.setNome("Mairon");
	//	responsavel.setNome("");
	//	responsavel.setMatricula("F1");
		
		return responsavel;
		
	}
	
	@Test
	public void cadastrarResponsavel () {
		
		Responsavel responsavelEsperado = this.getMock();
		responsavelEsperado.setCodigo(2l);
		
		Responsavel responsavelRetornado = null;
		try {
			responsavelRetornado = responsavelManager.cadastrar(this.getMock());
		} catch (ValidacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Assert.assertEquals(responsavelEsperado, responsavelRetornado);

		/*
		if (responsavelEsperado.equals(responsavelRetornado)) 
			System.out.println("igual");
		else System.out.println("diferente");*/
		
	}
	
}

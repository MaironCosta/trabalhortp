package rtp.model;

import java.util.Calendar;
import java.util.LinkedHashSet;

import rtp.exceptions.ValidacaoException;
import rtp.model.entity.Tarefa;
import rtp.model.entity.TarefaStatus;

public interface ITarefaManager extends IManager<Tarefa> {

	public LinkedHashSet<Tarefa> recuperarTarefaPorStatus(TarefaStatus tarefaStatus) throws ValidacaoException;
	
	public void fecharTarefa (long codigoTarefa, Calendar dtConclusao, String descricaoSolucao) throws ValidacaoException;
	
}

package rtp.model;

import rtp.exceptions.ValidacaoException;
import rtp.model.entity.Responsavel;

public interface IResponsavelManager extends IManager<Responsavel> {

	public Responsavel getByMatricula (String matricula) throws ValidacaoException;
	
}

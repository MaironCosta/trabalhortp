package rtp.model;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashSet;
import java.util.List;

import rtp.exceptions.ValidacaoException;
import rtp.model.entity.Tarefa;
import rtp.model.entity.TarefaStatus;
import rtp.repository.ITarefaRepository;
import rtp.repository.TarefaRepository;
import rtp.utils.UtilsData;
import rtp.utils.Validacoes;

public class TarefaManager implements ITarefaManager {

//	private static List<Tarefa> listaTarefas = new ArrayList<Tarefa>();
	
	private ITarefaRepository tarefaRepository;

	public TarefaManager() {
		// TODO Auto-generated constructor stub
		
		this.inicializar();
	}

	private void inicializar () {
		
		tarefaRepository = new TarefaRepository();
		
	}

	public Tarefa cadastrar (Tarefa t) throws ValidacaoException {

		this.validarCreate(t);
		
		t.setTarefaStatus(TarefaStatus.ABERTA);

	//	listaTarefas.add(t);
		tarefaRepository.save(t);

		return t;
	}
	
	public void validarCreate (Tarefa tarefa) throws ValidacaoException {
		
		List<String> erros = new ArrayList<>();

		if (Validacoes.isEmpty(tarefa.getTitulo())) {
			erros.add("Informe um título para tarefa");
		}
		
		if (tarefa.getDtPrazo() == null) {
			erros.add("Informe um prazo para tarefa");
		} else {
			
			try {
				
				if (UtilsData.isBefeoreToday(tarefa.getDtPrazo())) {
					erros.add("Prazo da tarefa inválido");
				}
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				erros.add("Prazo da tarefa inválido");
			}
			
		}		
		
		if (tarefa.getResponsavel() == null || tarefa.getResponsavel().getCodigo() == null) {
			erros.add("Informe o responsável pela tarefa");
		}
		
		if (tarefaRepository.getAll().contains(tarefa)) {
			erros.add("Tarefa já se encontra cadastrada");
		}
		
		if (!Validacoes.isEmpty(erros)) {
			
			throw new ValidacaoException(erros);
			
		}

	}
	
	public LinkedHashSet<Tarefa> recuperarTarefaPorStatus(TarefaStatus tarefaStatus) throws ValidacaoException  {
		
		if (tarefaStatus == null) {
			throw new ValidacaoException("Informe tipo da tarefa.");
		}

		LinkedHashSet<Tarefa> tarefasPorStatus = tarefaRepository.getByStatus(tarefaStatus);
		
		return tarefasPorStatus;
	}
	
	public void fecharTarefa (long codigoTarefa, Calendar dtConclusao, String descricaoSolucao) throws ValidacaoException {
		
		if (codigoTarefa == 0) {
			
			throw new ValidacaoException("Informe a tarefa a ser fechada.");
			
		}
		
	 /* LinkedHashSet<Tarefa> tarefas = tarefaRepository.getAll();
		
		for (Tarefa t : tarefas) {
			
			if (t.equals(tarefas)) {
				
				t.setTarefaStatus(TarefaStatus.FECHADAS);
				break;
			}
			
		}*/
		
		Tarefa tarefa = this.validarFechamento(codigoTarefa, dtConclusao, descricaoSolucao);
		
		tarefa.setDtConclusao(dtConclusao);
		tarefa.setDescricaoSolucao(descricaoSolucao);		
		tarefa.setTarefaStatus(TarefaStatus.CONCLUIDA);
		
	}

	private Tarefa validarFechamento(long codigoTarefa, Calendar dtConclusao, String descricaoSolucao) throws ValidacaoException {
		
		Tarefa tarefa = tarefaRepository.getByCodigo(codigoTarefa);
		
		List<String> erros = new ArrayList<String>();
		
		if (tarefa.getTarefaStatus().equals(TarefaStatus.CONCLUIDA)) {
			
			erros.add("Tarefa já se encontra fechada.");
			
		}
		
		if (dtConclusao == null) {
			
			erros.add("Data conclusao nao informada.");
			
		}
		
		if (Validacoes.isEmpty(descricaoSolucao)) {
			erros.add("Informe a soluçao encontrada.");
		}
		
		if (!Validacoes.isEmpty(erros)) {
			
			throw new ValidacaoException(erros);
			
		}
		
		return tarefa;
	}

	@Override
	public LinkedHashSet<Tarefa> getAll() {
		// TODO Auto-generated method stub
		return tarefaRepository.getAll();
	}

	@Override
	public Tarefa getByCodigo(long codigo) {
		// TODO Auto-generated method stub
		return tarefaRepository.getByCodigo(codigo);
	}

}
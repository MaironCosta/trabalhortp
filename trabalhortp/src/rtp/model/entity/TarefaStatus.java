package rtp.model.entity;

public enum TarefaStatus {

	ABERTA("ABERTA"),
	CONCLUIDA("CONCLUIDA");
	
	private String descricao;
	
	public String getDescricao() {
		return descricao;
	}
	
	private TarefaStatus (String descricao) {
		this.descricao = descricao;
	}

}

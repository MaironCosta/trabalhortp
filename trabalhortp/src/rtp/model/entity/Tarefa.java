package rtp.model.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import rtp.utils.UtilsData;

public class Tarefa {
	
	private Long codigo;
	private String titulo; // ok
	private String detalhes; // descricao tarefa -- ok
	private Calendar dtPrazo; // ok
	private Responsavel responsavel; // ok
	private boolean isUrgente; // ok
	private String descricaoSolucao;
	private Calendar dtConclusao;	

	private TarefaStatus tarefaStatus;
	
	public Tarefa() {
		// TODO Auto-generated constructor stub
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(String detalhes) {
		this.detalhes = detalhes;
	}

	public Calendar getDtPrazo() {
		return dtPrazo;
	}

	public void setDtPrazo(Calendar dtPrazo) {
		this.dtPrazo = dtPrazo;
	}

	public Responsavel getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Responsavel responsavel) {
		this.responsavel = responsavel;
	}

	public boolean isUrgente() {
		return isUrgente;
	}

	public void setUrgente(boolean isUrgente) {
		this.isUrgente = isUrgente;
	}

	public String getDescricaoSolucao() {
		return descricaoSolucao;
	}

	public void setDescricaoSolucao(String descricaoSolucao) {
		this.descricaoSolucao = descricaoSolucao;
	}

	public Calendar getDtConclusao() {
		return dtConclusao;
	}

	public void setDtConclusao(Calendar dtConclusao) {
		this.dtConclusao = dtConclusao;
	}

	public TarefaStatus getTarefaStatus() {
		return tarefaStatus;
	}

	public void setTarefaStatus(TarefaStatus tarefaStatus) {
		this.tarefaStatus = tarefaStatus;
	}

	public List<?> recuperarTarefaPorStatus(String status) {

		List<String> tarefas = new ArrayList<String>();
		if ( status.equals("abertas") ) {
			tarefas.add("Comprar sabăo");
			tarefas.add("Fazer unha");
		} else if ( status.equals("fechadas") ) {
			tarefas.add("Fazer escova");
			tarefas.add("Receber massagem");
		}
		return (tarefas);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tarefa other = (Tarefa) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Tarefa [codigo=" + codigo + ", titulo=" + titulo + ", dtPrazo="
				+ UtilsData.dataToString(dtPrazo) + ", responsavel=" + responsavel + ", tarefaStatus="
				+ tarefaStatus + "]";
	}
	
}

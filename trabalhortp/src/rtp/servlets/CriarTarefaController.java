package rtp.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import rtp.exceptions.ValidacaoException;
import rtp.model.IResponsavelManager;
import rtp.model.ResponsavelManager;
import rtp.model.TarefaManager;
import rtp.model.entity.Responsavel;
import rtp.model.entity.Tarefa;
import rtp.utils.UtilsData;

/**
 * Servlet implementation class CriarTarefa
 */
public class CriarTarefaController extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CriarTarefaController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	request.getRequestDispatcher("index.jsp").forward(request, response);
		
		IResponsavelManager responsavelManager = new ResponsavelManager();
		List<Responsavel> responsavels = new ArrayList<Responsavel>();
		responsavels.addAll(responsavelManager.getAll());
		
		request.setAttribute("responsavels", responsavels);
		
		request.getRequestDispatcher("criarTarefa.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String titulo = request.getParameter("titulo");
		String dtPrazo = request.getParameter("dtPrazo");
		String urgente = request.getParameter("urgente");
		String matriculaResponsavel = request.getParameter("matriculaResponsavel");
		String descricaoTarefa = request.getParameter("descricaoTarefa");
		
		TarefaManager tarefaManager = new TarefaManager();
		IResponsavelManager responsavelManager = new ResponsavelManager();

		String msg = "";
		try {

			Tarefa t = new Tarefa();

			t.setTitulo(titulo);
			t.setDtPrazo(UtilsData.stringByCalendar(dtPrazo));
			t.setUrgente(("on".equals(urgente))?true:false);			
			t.setResponsavel(responsavelManager.getByMatricula(matriculaResponsavel));
            t.setDetalhes(descricaoTarefa);
			
			tarefaManager.cadastrar(t);
			msg = "Tarefa criada com sucesso";

		} catch (ValidacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			msg = e.getErros().get(0);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			msg = "Erro no prazo."; 
		}
		
		request.setAttribute("msgStatus", msg);

		request.getRequestDispatcher("tarefaCriada.jsp").forward(request, response);

	}

}
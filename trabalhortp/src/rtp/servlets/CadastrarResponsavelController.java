package rtp.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import rtp.exceptions.ValidacaoException;
import rtp.model.IResponsavelManager;
import rtp.model.ResponsavelManager;
import rtp.model.entity.Responsavel;

/**
 * Servlet implementation class CriarTarefa
 */
public class CadastrarResponsavelController extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CadastrarResponsavelController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		IResponsavelManager responsavelManager = new ResponsavelManager();
		request.setAttribute("responsaveis", responsavelManager.getAll());

		request.getRequestDispatcher("cadastrarResponsavel.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String nomeResponsavel = request.getParameter("nomeResponsavel");
		
		Responsavel responsavel = new Responsavel();
		responsavel.setNome(nomeResponsavel);
		
		IResponsavelManager responsavelManager = new ResponsavelManager();
		
		try {
			
			responsavelManager.cadastrar(responsavel);
			
			request.setAttribute("msg", "Cadastrado com sucesso!");
			
		} catch (ValidacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			request.setAttribute("msg", e.getErros().get(0));
			
		}
		
		this.doGet(request, response);
	}

}
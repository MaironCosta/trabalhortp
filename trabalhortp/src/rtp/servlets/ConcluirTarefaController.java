package rtp.servlets;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import rtp.exceptions.ValidacaoException;
import rtp.model.ITarefaManager;
import rtp.model.TarefaManager;
import rtp.model.entity.Tarefa;
import rtp.utils.UtilsData;

/**
 * Servlet implementation class CriarTarefa
 */
public class ConcluirTarefaController extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConcluirTarefaController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String codigoTarefa = request.getParameter("codigoTarefa");
		
		ITarefaManager tarefaManager = new TarefaManager();
		
		Tarefa tarefa = tarefaManager.getByCodigo(Long.parseLong(codigoTarefa));
		
		request.setAttribute("tarefa", tarefa);
		request.setAttribute("concluir", true);
		
		request.getRequestDispatcher("detalharTarefa.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String codigoTarefa = request.getParameter("codigoTarefa");
		String dtConclusao = request.getParameter("dtConclusao");
		String descricaoSolucao = request.getParameter("descricaoSolucaoTarefa");
		
		ITarefaManager tarefaManager = new TarefaManager();
		
		try {
			
			tarefaManager.fecharTarefa(Long.parseLong(codigoTarefa), UtilsData.stringByCalendar(dtConclusao), descricaoSolucao);
			
			request.setAttribute("codigoTarefa", codigoTarefa);
			
			new DetalharTarefaController().doGet(request, response);
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ValidacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			request.setAttribute("codigoTarefa", codigoTarefa);
			
			request.setAttribute("msg", e.getErros().get(0));
			
			this.doGet(request, response);
			
		}	
		
	}

}
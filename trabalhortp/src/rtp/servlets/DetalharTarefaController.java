package rtp.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import rtp.model.ITarefaManager;
import rtp.model.TarefaManager;
import rtp.model.entity.Tarefa;

/**
 * Servlet implementation class CriarTarefa
 */
public class DetalharTarefaController extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetalharTarefaController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String codigoTarefa = request.getParameter("codigoTarefa");
		
		ITarefaManager tarefaManager = new TarefaManager();
		
		Tarefa tarefa = tarefaManager.getByCodigo(Long.parseLong(codigoTarefa));
		
		request.setAttribute("tarefa", tarefa);
		
		request.getRequestDispatcher("detalharTarefa.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		this.doGet(request, response);
		
	}

}
package rtp.repository;

import java.util.LinkedHashSet;

import rtp.model.entity.Responsavel;

public class ResponsavelRepository extends GenericRepository<Responsavel> implements IResponsavelRepository {
	
	private static long codigo;
	private static LinkedHashSet<Responsavel> listaResponsaveis = new LinkedHashSet<Responsavel>();

	public ResponsavelRepository() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Responsavel save(Responsavel responsavel) {
		// TODO Auto-generated method stub
		
		responsavel.setCodigo(++codigo);
		responsavel.setMatricula("F" + responsavel.getCodigo());
		listaResponsaveis.add(responsavel);
		
		return responsavel;
	}

	@Override
	public void remover(Responsavel responsavel) {
		// TODO Auto-generated method stub

		listaResponsaveis.remove(responsavel);

	}

	@Override
	public Responsavel getByCodigo(long codigoResponsavel) {
		// TODO Auto-generated method stub
		
		for (Responsavel responsavel : listaResponsaveis) {

			if (responsavel.getCodigo().longValue() == codigoResponsavel) {
				return responsavel;
			}
		}

		return null;
	}

	@Override
	public LinkedHashSet<Responsavel> getAll() {
		// TODO Auto-generated method stub
		return listaResponsaveis;
	}
	
	public Responsavel getByMatricula (String matricula) {		

		for (Responsavel responsavel : listaResponsaveis) {

			if (responsavel.getMatricula().trim().equalsIgnoreCase(matricula.trim())) {
				return responsavel;
			}
		}

		return null;
		
	}

}

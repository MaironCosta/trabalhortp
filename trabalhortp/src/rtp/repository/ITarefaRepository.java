package rtp.repository;

import java.util.LinkedHashSet;

import rtp.model.entity.Tarefa;
import rtp.model.entity.TarefaStatus;

public interface ITarefaRepository extends IGenericRepository<Tarefa> {

	public LinkedHashSet<Tarefa> getByStatus(TarefaStatus tarefaStatus);
	
}

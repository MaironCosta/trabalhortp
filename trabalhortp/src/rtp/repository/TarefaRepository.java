package rtp.repository;

import java.util.LinkedHashSet;

import rtp.model.entity.Tarefa;
import rtp.model.entity.TarefaStatus;

public class TarefaRepository extends GenericRepository<Tarefa> implements ITarefaRepository {

	private static long codigo;
	private static LinkedHashSet<Tarefa> listaTarefas = new LinkedHashSet<Tarefa>();
	
	public TarefaRepository() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Tarefa save(Tarefa tarefa) {
		// TODO Auto-generated method stub
		
		tarefa.setCodigo(++codigo);
		listaTarefas.add(tarefa);
		
		return tarefa;
	}
	
	public LinkedHashSet<Tarefa> getByStatus(TarefaStatus tarefaStatus) {
		
		LinkedHashSet<Tarefa> tarefasPorStatus = new LinkedHashSet<Tarefa>();

		for (Tarefa t : listaTarefas) {

			if (t.getTarefaStatus().equals(tarefaStatus)) {
				tarefasPorStatus.add( t );
			}

		}

		return tarefasPorStatus;
	}
	
	@Override
	public void remover(Tarefa t) {
		// TODO Auto-generated method stub

		listaTarefas.remove(t);

	}
	
	@Override
	public Tarefa getByCodigo(long codigoTarefa) {
		// TODO Auto-generated method stub
		
		for (Tarefa t : listaTarefas) {

			if (t.getCodigo().longValue() == codigoTarefa) {
				return t;
			}
		}

		return null;
	}
	
	@Override
	public LinkedHashSet<Tarefa> getAll() {
		// TODO Auto-generated method stub
		return listaTarefas;
	}

}

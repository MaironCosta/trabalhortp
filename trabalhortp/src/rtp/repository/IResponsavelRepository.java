package rtp.repository;

import rtp.model.entity.Responsavel;

public interface IResponsavelRepository extends IGenericRepository<Responsavel> {

	public Responsavel getByMatricula (String matricula);
	
}

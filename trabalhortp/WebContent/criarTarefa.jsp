<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
       <%--     <title>${nomePagina}</title> --%>
       
 <jsp:include page="head.jsp" />
 
<script type="text/javascript">

$(document).ready(function(){
	$("#datepicker").datepicker({
	    dateFormat: 'dd/mm/yy',
	    dayNames: ['Domingo','Segunda','Ter�a','Quarta','Quinta','Sexta','S�bado'],
	    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
	    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S�b','Dom'],
	    monthNames: ['Janeiro','Fevereiro','Mar�o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
	    nextText: 'Pr�ximo',
	    prevText: 'Anterior'
	});
	/* $("#datepicker").datepicker({
		inline: true
	}); */
	
});

</script>
       
</head>

<body>

<jsp:include page="menu.jsp" />

<h1>Criar tarefa</h1>

<form method="post" action="CriarTarefa.do">

Preencha os dados da tarefa:<p>

T�tulo:
<input type="text" name="titulo" value="" maxlength="100" size="40"/>
<br/>
Prazo:
<input type="text" id="datepicker" name="dtPrazo" readonly="readonly" value="" maxlength="10" size="10"/>
<br/>
Urgente?:
<input type="checkbox" name="urgente"/>
<br/>
Respons�vel:
<select name="matriculaResponsavel" size="1">
    <c:forEach items="${responsavels}" var="responsavel">
         <option value="${responsavel.matricula}"> ${responsavel.nome} </option>
    </c:forEach>
<!-- 	<option value="chaves"> Chaves </option> -->
<!--    <option value="quico"> Quico </option> -->
<!--    <option value="chiquinha"> Chiquinha </option> -->
</select>
<br/>

Descri�ao Tarefa:<br/>
<textarea cols="70" rows="5" name="descricaoTarefa"> </textarea>
<br/>

<br><br>

<center>
  <input type="SUBMIT">
</center>

</form>
<br></br>
<a href="index.jsp">P�gina inicial</a>
</body>
</html>
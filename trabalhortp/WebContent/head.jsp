<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${nomePagina}</title> 
<link href="<c:url value="css/container.css" />" rel="stylesheet">
<link href="<c:url value="css/jquery-ui-1.10.3.custom.css" />" rel="stylesheet">
<script src="<c:url value="js/jquery-1.9.1.js" />"></script>
<script src="<c:url value="js/jquery-ui-1.10.3.custom.js" />"></script>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
       <%--     <title>${nomePagina}</title> --%>
       
 <jsp:include page="head.jsp" />
 
<script type="text/javascript">

$(document).ready(function(){
	$("#datepicker").datepicker({
	    dateFormat: 'dd/mm/yy',
	    dayNames: ['Domingo','Segunda','Ter�a','Quarta','Quinta','Sexta','S�bado'],
	    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
	    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S�b','Dom'],
	    monthNames: ['Janeiro','Fevereiro','Mar�o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
	    nextText: 'Pr�ximo',
	    prevText: 'Anterior'
	});
	/* $("#datepicker").datepicker({
		inline: true
	}); */
	
});

</script>
       
</head>

<body>

<jsp:include page="menu.jsp" />

<h1>Cadastrar Respons�vel</h1>

<c:if test="${not empty msg}">
      ---------------------------------------------- <br/>
      ${msg} <br/>
      ---------------------------------------------- <br/>
</c:if>

<form method="post" action="CadastrarResponsavel.do">

Preencha os dados:<p>

Nome:
<input type="text" name="nomeResponsavel" value="" maxlength="100" size="40"/>
<br/>
<br><br>

<center>
  <input type="SUBMIT">
</center>

<hr size="2" width="70%">
<h1 align="center"> Respons�veis</h1>

<table width="50%" align="center" border="1">
       <thead>
              <tr>
                  <th> Nome </th>
                  <th> Matr�cula </th>
              </tr>
       
       </thead>
       <tbody>
              <c:forEach var="r" items="${responsaveis}">
                  <tr>
                      <td> ${r.nome} </td>
                      <td> ${r.matricula} </td>
                  </tr>
              </c:forEach>
       </tbody>
</table>

</form>
<br></br>
<a href="index.jsp">P�gina inicial</a>
</body>
</html>
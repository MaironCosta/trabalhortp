<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
      <jsp:include page="head.jsp" />
      
<script type="text/javascript">

$(document).ready(function(){
	$("#datepicker").datepicker({
	    dateFormat: 'dd/mm/yy',
	    dayNames: ['Domingo','Segunda','Ter�a','Quarta','Quinta','Sexta','S�bado'],
	    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
	    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S�b','Dom'],
	    monthNames: ['Janeiro','Fevereiro','Mar�o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
	    nextText: 'Pr�ximo',
	    prevText: 'Anterior'
	});
	/* $("#datepicker").datepicker({
		inline: true
	}); */
	
});

</script>
</head>

<body>

<jsp:include page="menu.jsp" />

<h1>Detalhar Tarefa</h1>

<c:if test="${not empty msg}">
      ---------------------------------------------- <br/>
      ${msg} <br/>
      ---------------------------------------------- <br/>
</c:if>

<form method="post" action="ConcluirTarefa.do">

Dados da tarefa:<p>

<input type="hidden" name="codigoTarefa" value="${tarefa.codigo}" />

T��tulo: ${tarefa.titulo}
<br/>
Prazo: <fmt:formatDate value="${tarefa.dtPrazo.time}" pattern="dd/MM/yyyy"/>
<br/>
<c:if test="${tarefa.tarefaStatus eq 'CONCLUIDA'}">
   <% try { %>
   Data Conclus�o: <fmt:formatDate value="${tarefa.dtConclusao.time}" pattern="dd/MM/yyyy"/>
   <% } catch (Exception e) {
      
	   e.printStackTrace();
   
   }   %>
   
   <br/>
   Descri�ao Solu��o:${tarefa.descricaoSolucao}
   <br/>
</c:if>
Urgente: 
<c:if test="${tarefa.urgente eq true}"> SIM </c:if>
<c:if test="${tarefa.urgente eq false}"> N�O </c:if> 
<br/>
Respons�vel: ${tarefa.responsavel.matricula} - ${tarefa.responsavel.nome}
<br/>

Descri��o Tarefa:<br/>
<c:choose>
   <c:when test="${empty tarefa.detalhes}">N�O POSSUI</c:when>
 <c:otherwise> ${tarefa.detalhes} </c:otherwise>
</c:choose>
<br/>

<c:if test="${concluir eq true}">

   Data Conclus�o:
   <input type="text" id="datepicker" name="dtConclusao" readonly="readonly" value="" maxlength="10" size="10"/>
   <br/>

   Descri�ao Solu��o:<br/>
   <textarea cols="70" rows="5" name="descricaoSolucaoTarefa"> </textarea>
   <br/>
   
   <center>
     <input type="SUBMIT" value="Resolver">
   </center>

</c:if>

<br><br>


</form>
<br></br>
<a href="index.jsp">P�gina inicial</a>
</body>
</html> 
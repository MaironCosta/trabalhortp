<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
   <jsp:include page="head.jsp" />
</head>

<body>
<jsp:include page="menu.jsp" />
<h1>Lista de tarefas</h1>
<p/>

<table border="1" width="75%">
       <thead>
              <tr>
                  <th> A��es </th>
                  <th> Titulo </th>
                  <th> Respons�vel </th>
                  <th> Prazo </th>
                  <th> Urgente </th>
                  <th> Status </th>
              </tr>
       </thead>
       <tbody>
              <c:forEach items="${tarefas}" var="t">
                  <tr>
                      <td> 
                           <c:if test="${t.tarefaStatus.descricao eq 'ABERTA'}"> 
                              <a href="<c:url value="ConcluirTarefa.do?codigoTarefa=${t.codigo}"/>">
                                 RESOLVER
                              </a> /
                           </c:if> 
                           <a href="<c:url value="DetalharTarefa.do?codigoTarefa=${t.codigo}"/>" target="_blank"> DETALHAR </a>
                      </td>
                      <td>${t.titulo}</td>
                      <td>${t.responsavel.nome}</td>
                      <td> <fmt:formatDate value="${t.dtPrazo.time}" pattern="dd/MM/yyyy" />  </td>
                      <td> 
                           <c:if test="${t.urgente eq true}">  
                                 SIM
                           </c:if>
                           <c:if test="${t.urgente eq false}">  
                                 NAO
                           </c:if>                      
                      </td>
                      <td> ${t.tarefaStatus.descricao} </td>
                  </tr>
              </c:forEach>
       </tbody>
</table>
</body>
</html>